<?php
// require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Nama : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs <br>"; // 4
echo "cold_blooded : $sheep->cold_blooded <br>"; // "no"
echo "<br><br>";

$kodok = new Frog("buduk");

echo "Nama : $kodok->name <br>";
echo "legs : $kodok->legs <br>";
echo "cold_blooded : $kodok->cold_blooded <br>";
$kodok->jump(); 

echo "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Nama : $sungokong->name <br>";
echo "legs : $sungokong->legs <br>";
echo "cold_blooded : $sungokong->cold_blooded <br>";
$sungokong->yell();
?>